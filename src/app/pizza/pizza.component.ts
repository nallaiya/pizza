import { Component, OnInit, TemplateRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
@Component({
  selector: 'app-pizza',
  templateUrl: './pizza.component.html',
  styleUrls: ['./pizza.component.css']
})
export class PizzaComponent implements OnInit {
  list: any[] = [];
  persionList: any[] = [];
  modalRef: BsModalRef;
  constructor(private http: HttpClient, private modalService: BsModalService) {
    this.http.get('assets/pizza.json').subscribe((res: any) => {
      this.list = res.list;

    })
  }

  openModal(template: TemplateRef<any>, i) {
    this.persionList = [];
    this.modalRef = this.modalService.show(template);
    this.persionList.push(i);

  }
  ngOnInit(): void {

  }

}
