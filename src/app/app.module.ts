import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { PizzaComponent } from './pizza/pizza.component';
import { HttpClientModule } from '@angular/common/http';
import { ModalModule } from 'ngx-bootstrap/modal';
const routes: Routes = [
  {
    path: '',
    component:PizzaComponent

  }
];

@NgModule({
  declarations: [
    AppComponent,
    PizzaComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ModalModule,
    RouterModule.forRoot(routes),
    ModalModule.forRoot(),
  ],
  exports: [RouterModule],
  bootstrap: [AppComponent]
})
export class AppModule { }
